class Api::V1::BancosController < ApplicationController
  # GET /bancos
  # GET /bancos.json
  def index
    @bancos = Banco.all

    respond_to do |format|
      format.json { render json: @bancos }
      format.xml { render xml: @bancos }
    end
  end

  # GET /bancos/1
  # GET /bancos/1.json
  def show
    @banco = Banco.find(params[:id])

    respond_to do |format|
      format.json { render json: @banco }
      format.xml { render xml: @banco }
    end
  end

  def plazos_y_tasas
    @banco = Banco.find(params[:id])
    @plazos = @banco.tasas.order('plazo')
    respond_to do |format|
      format.json { render json: @plazos }
      format.xml { render xml: @plazos }
    end
  end

  def tasas
    @tasa = Tasa.find(params[:plazo_id])
    respond_to do |format|
      format.json { render json: @tasa }
      format.xml { render xml: @tasa }
    end
  end

  def bancos_completo
    #regresa el listado de bancos y las tasas
    @bancos = Banco.banco_completo
    respond_to do |format|
      format.json { render json: @bancos.to_json(include: :tasas), status: :ok }
      format.xml { render xml: @bancos.to_xml(include: :tasas), status: :ok }
    end
  end

end
