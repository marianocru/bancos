class Banco < ActiveRecord::Base
  has_many :tasas

  attr_accessible :nombre, :tasa

  validates_presence_of :nombre, :message => 'Es requerido.'
  validates_uniqueness_of :nombre, :message => "Repetido "


  def self.banco_completo
    #regresa el listado de bancos y las tasas
    Banco.includes(:tasas).order('bancos.nombre, tasas.plazo').all

  end
end
