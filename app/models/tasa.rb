class Tasa < ActiveRecord::Base
  belongs_to :banco
  attr_accessible :plazo, :tasa, :banco_id
  validates_presence_of :plazo, :banco_id, :tasa, :message => 'Es requerido.'
  validates_uniqueness_of :plazo, :scope => [:banco_id], :message => "Repetido "

  def self.meses_a_dias(meses = 0)
    #convierte la cantidad de meses en días
    #tomando como base que las tasa se definen
    #mayormente cada 30 días (30, 60, 90...)
    meses.to_i * 30
  end

end
