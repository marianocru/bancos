# Bancos

Para instalar:

# Pre requisitos:
ruby 1.9.3-p547
raisl  '3.2.21'
Bundle

# Descargar o clonar la aplicación desde https://marianocru@bitbucket.org/marianocru/bancos.git

git clone https://marianocru@bitbucket.org/marianocru/bancos.git

# Ejecutar bundle

rails bundle

#Crear la base de datos
rake db:create

#Correr las migraciones
rake db:migrate



