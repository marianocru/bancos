class CreateBancos < ActiveRecord::Migration
  def change
    create_table :bancos do |t|
      t.string :nombre
      t.decimal :tasa, precision: 10, scale: 2

      t.timestamps
    end
  end
end
