class RemoveTasaFromBancos < ActiveRecord::Migration
  def up
    remove_column :bancos, :tasa
  end

  def down
    add_column :bancos, :tasa, :string
  end
end
