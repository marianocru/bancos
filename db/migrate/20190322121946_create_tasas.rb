class CreateTasas < ActiveRecord::Migration
  def change
    create_table :tasas do |t|
      t.references :banco
      t.integer :plazo
      t.decimal :tasa, precision: 10, scale: 2

      t.timestamps
    end
    add_index :tasas, :banco_id
  end
end
